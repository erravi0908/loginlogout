/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    View,
    Text, StyleSheet
} from 'react-native';



const Header = (props) => {
    return (
        <View style={styles.header}>
            <Text style={styles.text}>{props.title} </Text>
        </View >
    )
}

Header.defaultProps = {
    title: "SHOPPING LIST"
}



const styles = StyleSheet.create({

    header: {

        height: 60, padding: 15,
        backgroundColor: 'red',

    }
    ,
    text:
    {

        color: '#fff',
        fontSize: 23,
        textAlign: 'center'
    },

    img: {
        width: 100, height: 100, borderRadius: 100 / 2
    }




})



export default Header;
