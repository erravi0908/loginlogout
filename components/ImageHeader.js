/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  Text,StyleSheet,Image
} from 'react-native';


const ImageHeader = () => {
  return (
    <View style={styles.container }>
    
     <Text style={styles.textStyle }>Hello World</Text>
     <Image source={{uri:"https://randomuser.me/api/portraits/men/1.jpg"}} style={styles.img}/>

      

    </View >
  )
}

const styles=StyleSheet.create({

  container:{

    flex:1,backgroundColor:'white',justifyContent:'center', alignItems:'center'
  }
,
    textStyle:
    {
      
      color:'blue',
      fontSize:20
    },

    img:{
      width:100,height:100,borderRadius:100/2
    }
  



})



export default ImageHeader;
