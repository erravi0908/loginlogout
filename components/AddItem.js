/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useState} from 'react';
import {
    View,
    Text, StyleSheet, TextInput, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';



const AddItem = ({ addItem}) => {

const [text,setText]=useState(' ');


const onChange=(textValue)=> {setText(textValue) }

    return (
        <View>
            <TextInput placeholder="Add items...." style={styles.input}  onChangeText={onChange}/>
            <TouchableOpacity style={styles.btn} onPress={()=>addItem(text)} >
                <Text style={styles.btntext} >
                    <Icon name="plus" size={20} > Add Item </Icon>
                </Text>
            </TouchableOpacity>
        </View>

    )
}


const styles = StyleSheet.create({

    input: {
        height: 60,
        padding: 8,
        fontSize: 16

    },
    btn: {
        backgroundColor: '#c2bad8',
        padding: 9,
        margin: 5

    },
    btntext: {
        color: 'green',
        fontSize: 20,
        textAlign: 'center'

    }




})



export default AddItem;
