/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React ,{useState}from 'react';
import {
  View,
  StyleSheet,FlatList
} from 'react-native';

import Header from './components/Header';
import ImageHeader from './components/ImageHeader';
import ListItems from './components/ListItems';
import AddItem from  './components/AddItem';
import uuid from 'react-native-uuid';


const App = () => {


  // state of the componente
const [list,setList]=useState([
  {id:uuid.v1(),text:'Milk'},{id:uuid.v1(),text:'Biscuit'},{id:uuid.v1(),text:'Fruits'},{id:uuid.v1(),text:'Grocery'}
  
  ]);

  // delete item method
  const deleteItem=(id)=>
{ //setting the filtered list back into the state
  setList(prevItems =>{
  return prevItems.filter(item=>item.id!=id);
  });
}

// add item method
const addItem=(text)=>
{ //setting the filtered list back into the state

setList(prevItems=>{
return [{id:uuid.v1(),text},...prevItems]

})

}

  return (

    <View style={styles.container}>
      <Header/>
      <AddItem  addItem={addItem}/>
     {/* using flatlist to render the 'list'  and then passing the item and deleteItem() method to <ListItems> component  */}
    <FlatList  data={list} renderItem={({item})=><ListItems item={item} deleteItem={deleteItem} />} />
  
     <ImageHeader/> 
    </View>
  )
}

const styles = StyleSheet.create({

  container: {

      flex:1,
      paddingTop:20

  }
  ,
  textStyle:
  {

    color: 'blue',
    fontSize: 20
  },

  



})



export default App;
